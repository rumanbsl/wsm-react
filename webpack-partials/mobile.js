const path = require('path')
const projectRoot = path.resolve(__dirname).replace('webpack-partials', '')
const plugins = require('./plugins')
const rules = require('./module')
// const { externals, resolveLoader } = require('./externals');
const { alias } = require('./env-variables')
const phoneGap = {
  resolve: {
    alias
  },
  entry: path.resolve(projectRoot, 'src', 'index.js'),
  target: 'web',
  output: {
    path: path.resolve(projectRoot, 'www'),
    filename: 'index.js'
  },
  module: {
    rules
  },
  plugins,
  cache: true
}

module.exports = phoneGap
