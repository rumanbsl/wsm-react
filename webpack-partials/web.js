const path = require('path')
const plugins = require('./plugins')
const rules = require('./module')
const { production, development, desktop, mobile, web } = require('./env-variables')
const projectRoot = path.resolve(__dirname).replace('webpack-partials', '')
const webpack = require('webpack')
const UglifyJS = require('uglifyjs-webpack-plugin')
const nodeExternals = require('webpack-node-externals')
const { alias } = require('./externals')
const front = {
  resolve: {
    alias
  },
  devtool: development ? '#source-map' : false,
  entry: production
    ? [ path.resolve(projectRoot, 'src', 'index') ]
    : [
      'react-hot-loader/patch',
      'webpack-dev-server/client?http://localhost:8081/',
      'webpack/hot/only-dev-server',
      path.resolve(projectRoot, 'src', 'index')
    ],
  devServer: {
    hot: !production,
    quiet: true,
    port: 8081,
    historyApiFallback: true
  },
  output: {
    path: path.resolve(projectRoot, 'dist', 'web'),
    publicPath: '/',
    filename: 'index.js'
  },
  target: 'web',
  plugins: plugins,
  module: {
    rules: rules
  },
  cache: true
}

const back = {
  externals: [ nodeExternals(
    {
      // this WILL include `jquery` and `webpack/hot/dev-server` in the bundle, as well as `lodash/*`
      // whitelist: ['jquery', 'webpack/hot/dev-server', /^lodash/]
    }
  ) ],
  entry: [ path.resolve(projectRoot, 'src', 'server') ],
  output: {
    path: path.resolve(projectRoot, 'dist', 'web'),
    publicPath: '',
    filename: 'server.js'
  },
  target: 'node',
  node: {
    __dirname: false,
    __filename: false
  },
  plugins: [
    new webpack.DefinePlugin({
      desktop: JSON.stringify(desktop),
      mobile: JSON.stringify(mobile),
      production: JSON.stringify(production),
      development: JSON.stringify(development),
      web: JSON.stringify(web)
    })
  ],
  module: {
    rules: [ {
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: [ 'env' ]
        }
      }
    }]
  }
}

if (production) {
  back.plugins.push(
    new UglifyJS()
  )
}

if (development) {
  back.module.rules.push(
    {
      enforce: 'pre',
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'standard-loader'
    })
}
module.exports = { front, back }
