const { production, development } = require('./env-variables')
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin')

const postCSS = {
  loader: 'postcss-loader',
  options: {
    sourceMap: !!development,
    plugins: function () {
      return development
        ? [
          require('lost')()
        ]
        : [
          require('postcss-cssnext')(),
          require('lost')()
        ]
    }
  }
}

const CSSLoader = {
  loader: 'css-loader',
  options: {
    url: true,
    minimize: !development
  }
}

let rules = [
  {
    test: /\.(js|jsx)$/,
    exclude: /node_modules/,
    use: {
      loader: 'babel-loader',
      options: {
        presets: ['stage-0', 'react', 'env'],
        plugins: [
          'react-hot-loader/babel'
          // Enables React code to work with HMR.
        ]
      }
    }
  },
  {
    test: /\.(png|jpg|jpeg|bmp|svg)$/,
    use: [
      {
        loader: 'url-loader',
        options: {
          name: './images/[name].[ext]',
          publicPath: '.',
          limit: 10000
        }
      },
      {
        loader: 'image-webpack-loader',
        options: {
          mozjpeg: {
            quality: 65
          },
          pngquant: {
            quality: '10-20',
            speed: 4
          },
          svgo: {
            plugins: [
              {
                removeViewBox: false
              },
              {
                removeEmptyAttrs: false
              }
            ]
          },
          gifsicle: {
            optimizationLevel: 7,
            interlaced: false
          },
          optipng: {
            optimizationLevel: 7,
            interlaced: false
          }
        }
      }
    ]
    // exclude: /node_modules/
  },
  { test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, use: ['url?limit=10000&mimetype=application/font-woff&name=./fonts/[name]-[hash].[ext]'/*, exclude: /node_modules/ */] },
  { test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, use: ['url?limit=10000&mimetype=application/font-woff&name=./fonts/[name]-[hash].[ext]'/*, exclude: /node_modules/ */] },
  { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, use: ['url?limit=10000&mimetype=application/octet-stream&name=./fonts/[name]-[hash].[ext]'/*, exclude: /node_modules/ */] },
  { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, use: ['file?name=./fonts/[name]-[hash].[ext]'] }

]

if (development) {
  rules.push(
    {
      enforce: 'pre',
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      loader: 'standard-loader'
    },
    {
      test: /\.css$/,
      use: [
        'style-loader',
        CSSLoader,
        postCSS
      ]
      /* exclude: /node_modules/ */
    },
    {
      test: /\.(sass|scss)$/,
      use: [
        'style-loader',
        CSSLoader,
        postCSS,
        'resolve-url-loader',
        'sass-loader'
      ]
      /* exclude: /node_modules/ */
    }
  )
}

if (production) {
  rules.push(
    {
      test: /\.css$/,
      use: ExtractTextWebpackPlugin.extract([
        CSSLoader,
        postCSS]
      )
      /* exclude: /node_modules/ */
    },
    {
      test: /\.(sass|scss)$/,
      use: ExtractTextWebpackPlugin.extract([
        CSSLoader,
        postCSS,
        'resolve-url-loader',
        'sass-loader']
      )
      /* exclude: /node_modules/ */
    }
  )
}

module.exports = rules
