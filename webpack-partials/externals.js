const path = require('path')
const { production } = require('./env-variables')
const dir = (path.resolve(__dirname).replace('webpack-partials', ''))
const reactJS = production ? path.resolve(dir, 'node_modules', 'react', 'dist', 'react.min.js') : path.resolve(dir, 'node_modules', 'react', 'dist', 'react.js')
const dom = production ? path.resolve(dir, 'node_modules', 'react-dom', 'dist', 'react-dom.min.js') : path.resolve(dir, 'node_modules', 'react-dom', 'dist', 'react-dom.js')

module.exports = {
  alias: {
    react: reactJS,
    'react-dom': dom
  }
}
