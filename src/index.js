/* global  mobile */
/* eslint no-console: 0 */

import render from './render'
import App from './App/main.jsx'
import './App/images/imgs.jpg'
import './App/css/index.scss';

/*
  //enabling it casues HMR to break.
  // Commenting out during development for !mobile platforms

  import inMobile from './inMobile.js';
  if (mobile) inMobile();
*/

(function isHot () {
  if (!mobile) {
    if (module.hot) {
      module.hot.accept('./App/main.jsx', () => {
        render(App)
      })
    }
  }
})()

if (!mobile) render(App)
