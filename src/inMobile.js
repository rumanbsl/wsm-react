/* eslint no-console: 0 */
import render from './render'
import App from './App/main.jsx'
export default function inMobile () {
  'use strict'

  document.addEventListener('deviceready', onDeviceReady.bind(this), false)

  function onDeviceReady () {
    // Handle the Cordova pause and resume events
    plugins(navigator)
    render(App)
    document.addEventListener('pause', onPause.bind(this), false)
    document.addEventListener('resume', onResume.bind(this), false)
  }

  function onPause () {
    // TODO: This application has been suspended. Save application state here.
  }

  function onResume () {
    // TODO: This application has been reactivated. Restore application state here.
  }
}

function plugins ({ camera }) {
  console.log('camera functioning: ', Boolean(camera))
}
