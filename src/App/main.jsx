/* eslint no-console:0 */

/*
NOTE:
-> How to get query string from URL?
  ANS: new URLSearchParams( location.search ).get( STR_NAME )
-> Compent props contains match, locatioin, and history properties
-> match contains params of url
-> render can be occasionally rendered, but children always renders regardless of matched route or not
-> Switch is used for conditional rendering of component. If route is not matched a default render is used.
*/

import React, { Component } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'

import Posts from './Posts/Posts-container.js'
import Single from './Posts/Post-single-container.js'
import Store from './Store.js'

class App extends Component {
  render () {
    return (
      <Provider store={Store}>
        <BrowserRouter>
          <MuiThemeProvider>
            <div>
              <Switch>
                <Route exact path='/' component={Posts} />
                <Route exact path='/:id' component={Single} />

              </Switch>

            </div>
          </MuiThemeProvider>
        </BrowserRouter>
      </Provider>
    )
  }
}

export default App
