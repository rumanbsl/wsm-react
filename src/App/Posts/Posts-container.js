/* eslint react/prop-types:0 */

import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table'
import { connect } from 'react-redux'

import { fetchPosts } from './posts-actions'

class Posts extends Component {
  componentWillMount () {
    return this.props.fetchPosts()
  }

  render () {
    /* eslint no-console:0 */
    if (!this.props.posts) return <div>Loading...</div>

    return (
      <Table>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow>
            <TableHeaderColumn>ID</TableHeaderColumn>
            <TableHeaderColumn>User ID</TableHeaderColumn>
            <TableHeaderColumn>Title</TableHeaderColumn>
            <TableHeaderColumn>Post</TableHeaderColumn>
          </TableRow>
        </TableHeader>

        <TableBody displayRowCheckbox={false}>
          { this._renderLists(this.props.posts) }
        </TableBody>
      </Table>
    )
  }
  _renderLists ({ list }) {
    return list.map(item => {
      return (
        <TableRow key={item.id}>
          <TableRowColumn><NavLink to={'/' + item.id}>{ item.id }</NavLink></TableRowColumn>
          <TableRowColumn><NavLink to={'/' + item.id}>{ item.userId }</NavLink></TableRowColumn>
          <TableRowColumn><NavLink to={'/' + item.id}>{ item.title }</NavLink></TableRowColumn>
          <TableRowColumn><NavLink to={'/' + item.id}>{ item.body }</NavLink></TableRowColumn>
        </TableRow>
      )
    })
  }
}

function mapStateToProps (state) {
  return {
    posts: state.posts
  }
}

export default connect(mapStateToProps, { fetchPosts })(Posts)
