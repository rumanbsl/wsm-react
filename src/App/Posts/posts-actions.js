import axios from 'axios'
export const actionType = {
  FETCH_POSTS: 'FETCH_POSTS',
  FETCH_SINGLE: 'FETCH_SINGLE'
}

export const fetchPosts = () => {
  const request = axios.get('https://jsonplaceholder.typicode.com/posts')
  return {
    type: actionType.FETCH_POSTS,
    payload: request
  }
}

export const fetchSingle = (id) => {
  const request = axios.get('https://jsonplaceholder.typicode.com/posts/' + id)
  return {
    type: actionType.FETCH_SINGLE,
    payload: request
  }
}
