import { actionType } from './posts-actions'

export default function (state = { list: [], single: null }, action = null) {
  /* eslint-disable */
  switch ( action.type ) {
    case actionType.FETCH_POSTS:
      return {
        ...state, list: action.payload.data
      };
    case actionType.FETCH_SINGLE:
      return {
        ...state, single: action.payload.data
      };
    default: return state;
  }
  /* eslint-enable */
}
