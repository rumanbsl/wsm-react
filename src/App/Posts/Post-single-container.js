/* eslint react/prop-types:0 no-console:0 */
import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'

import { fetchSingle } from './posts-actions'
class PostSingleContainer extends Component {
  componentWillMount () {
    this.props.fetchSingle(this.props.match.params.id)
  }

  render () {
    if (!this.props.single) {
      return (
        <div>
          Loading...
      </div>
      )
    }

    return (
      <div>
        <NavLink to='/'> GO BACK </NavLink>
        <h2>Title: { this.props.single.title }</h2>
        <h4>Title: { this.props.single.body }</h4>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    single: state.posts.single
  }
}
export default connect(mapStateToProps, { fetchSingle })(PostSingleContainer)
