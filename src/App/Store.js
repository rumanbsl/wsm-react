import { combineReducers, createStore, applyMiddleware } from 'redux'
import reduxPromise from 'redux-promise'

import postsAction from './Posts/posts-reducers'

const storeWithMiddleware = applyMiddleware(reduxPromise)(createStore)

export default storeWithMiddleware(
  combineReducers({
    posts: postsAction
  })
)
